    text

kmain::
    lea.l   stack_end,a7    ; Reset stack to top of memory
    pea.l   (argv)
    move.l  #argc,-(a7)
    jsr     (main)


    data

argc = 0

argv
    dc.l    0
argv_end


    bss

stack
    ds.l    $1000
stack_end
