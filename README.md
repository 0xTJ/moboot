# Moboot Bootloader

This is an ELF SD bootloader for use with [rosco_m68k](https://rosco-m68k.com/).

## Building

```
make clean all
```

This will build `moboot.bin`, which can be uploaded to a board that is running the `serial-receive` firmware.
