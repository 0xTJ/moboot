#include "elf.h"
#include <sdfat.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif

extern char _end[];
extern char RAMLIMIT[];
const size_t stack_reserved = 0x400;

static bool phdr_load(Elf32_Phdr *phdr, FL_FILE *kernel);

int main() {
    printf("Moboot Bootloader\n");

    if (!SD_check_support()) {
        printf("ERROR: SD support not present.\n");
        return EXIT_FAILURE;
    }

    if (!SD_FAT_initialize()) {
        printf("ERROR: Failed to initialise SD FAT.\n");
        return EXIT_FAILURE;
    }

    const char *kernel_filename = "/mosys.elf";
    printf("Loading \"%s\"\n", kernel_filename);

    FL_FILE *kernel = fl_fopen(kernel_filename, "R\n");
    if (!kernel) {
        printf("ERROR: Failed to open \"%s\".\n", kernel_filename);
        return EXIT_FAILURE;
    }

    // Read in ELF header
    Elf32_Ehdr ehdr;
    int result;
    // This function has incorrect behaviour, it should return the count argument on success.
    // if ((result = fl_fread(&ehdr, sizeof ehdr, 1, kernel)) != 1) {
    if ((result = fl_fread(&ehdr, sizeof ehdr, 1, kernel)) != sizeof ehdr) {
        printf("ERROR: Failed to read ELF header. %d\n", result);
        return EXIT_FAILURE;
    }

    // Verify ELF identification
    if (ehdr.e_ident[EI_MAG0] != ELFMAG0 ||
        ehdr.e_ident[EI_MAG1] != ELFMAG1 ||
        ehdr.e_ident[EI_MAG2] != ELFMAG2 ||
        ehdr.e_ident[EI_MAG3] != ELFMAG3) {
        printf("ERROR: File \"%s\" is not an ELF file.\n", kernel_filename);
        return EXIT_FAILURE;
    }
    if (ehdr.e_ident[EI_CLASS] != ELFCLASS32) {
        printf("ERROR: Class %hhu not supported.\n", ehdr.e_ident[EI_CLASS]);
        return EXIT_FAILURE;
    }
    if (ehdr.e_ident[EI_DATA] != ELFDATA2MSB) {
        printf("ERROR: Data encoding %hhu not supported.\n", ehdr.e_ident[EI_DATA]);
        return EXIT_FAILURE;
    }
    if (ehdr.e_ident[EI_VERSION] != EV_CURRENT) {
        printf("ERROR: Version %hhu not supported.\n", ehdr.e_ident[EI_VERSION]);
        return EXIT_FAILURE;
    }

    // Verify type
    if (ehdr.e_type != ET_EXEC) {
        printf("ERROR: Type is not executable.\n");
        return EXIT_FAILURE;
    }

    // Verify machine
    if (ehdr.e_machine != EM_68K) {
        printf("ERROR: Machine is not Motorola 68000.\n");
        return EXIT_FAILURE;
    }

    // Verify version
    if (ehdr.e_version != ehdr.e_ident[EI_VERSION]) {
        printf("ERROR: Version mismatch.\n");
        return EXIT_FAILURE;
    }

    // Verify that an entry exists
    if (ehdr.e_entry == 0) {
        printf("ERROR: No entry point present.\n");
        return EXIT_FAILURE;
    }

    // Verify ELF header size
    if (ehdr.e_ehsize != sizeof(ehdr)) {
        printf("ERROR: Unexpected ELF header size.\n");
        return EXIT_FAILURE;
    }

    // Read program headers
    if (ehdr.e_phoff == 0) {
        printf("ERROR: No program headers present.\n");
        return EXIT_FAILURE;
    }
    if (fl_fseek(kernel, ehdr.e_phoff, SEEK_SET) != 0) {
        printf("ERROR: Failed to seek to program headers offset.\n");
        return EXIT_FAILURE;
    }
    if (ehdr.e_phentsize != sizeof(Elf32_Phdr)) {
        printf("ERROR: Unexpected program header size.\n");
        return EXIT_FAILURE;
    }
    Elf32_Phdr phdrs[ehdr.e_phnum];
    // This function has incorrect behaviour, it should return the count argument on success.
    // if (fl_fread(phdrs, ehdr.e_phentsize, ehdr.e_phnum, kernel) != ehdr.e_phnum) {
    if (fl_fread(phdrs, ehdr.e_phentsize, ehdr.e_phnum, kernel) != ehdr.e_phentsize * ehdr.e_phnum) {
        printf("ERROR: Failed to read program headers.\n");
        return EXIT_FAILURE;
    }

    // Process program headers
    for (size_t phidx = 0; phidx < ehdr.e_phnum; ++phidx) {
        switch (phdrs[phidx].p_type) {
        case PT_NULL:
            break;
        case PT_LOAD:
            if (!phdr_load(&phdrs[phidx], kernel)) {
                return EXIT_FAILURE;
            }
            break;
        default:
            break;
        }
    }

    ((void (*)()) ehdr.e_entry)();
    
    return EXIT_FAILURE;
}

static bool phdr_load(Elf32_Phdr *phdr, FL_FILE *kernel) {
    // Seek to this segment's offset in file
    if (fl_fseek(kernel, phdr->p_offset, SEEK_SET) != 0) {
        printf("ERROR: Failed to seek to segment offset.\n");
        return false;
    }

    // Without an MMU, virtual address must match physical address
    if (phdr->p_vaddr != phdr->p_paddr) {
        printf("ERROR: Virtual address does not match physical address without MMU.\n");
        return false;
    }

    // Check segment alignment
    if (phdr->p_vaddr % phdr->p_align != phdr->p_offset % phdr->p_align) {
        printf("ERROR: Incorrect segment alignment.\n");
        return false;
    }

    // Check segment sizes
    if (phdr->p_memsz <= phdr->p_filesz) {
        printf("ERROR: Segment file size is larger than memory size.\n");
        return false;
    }

    void *start = (void *) phdr->p_vaddr;
    void *end = (void *) (phdr->p_vaddr + phdr->p_memsz);

    printf("Loading segment at [%p, %p)\n", start, end);

    // Ensure bootloader isn't clobbered
    if ((char *) start < _end) {
        printf("ERROR: Refusing to load segment that will clobber bootloader.\n");
        return false;
    }

    // Read segment's data
    if (fl_fread((void *) phdr->p_vaddr, 1, phdr->p_filesz, kernel) != (int) phdr->p_filesz) {
        printf("ERROR: Failed to read segment.\n");
        return false;
    }

    // Clear segment's remaining memory
    memset((void *) (phdr->p_vaddr + phdr->p_filesz), 0, phdr->p_memsz - phdr->p_filesz);

    return true;
}
